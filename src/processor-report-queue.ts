import fs from 'node:fs'
import path from 'node:path'

import { Job } from 'bull'

import { env } from './env'
import { axiosService } from './lib/axios'
import { KafkaSendMessage } from './lib/kafka/producer'
import { ReportQueueData } from './lib/queue'

const REPORTS_PATH = path.join(__dirname, 'xlsx-reports')

export default async function (job: Job<ReportQueueData>) {
  const kafkaProducer = new KafkaSendMessage()

  const { data } = job
  const { payload, headers, deliveryTo } = data
  const { authorization, refreshToken } = headers
  const { filters, reportPathName } = payload

  const url = new URL(reportPathName, env.DATALIFE_API)
  Object.keys(filters).forEach((key) =>
    url.searchParams.append(key, filters[key]),
  )

  try {
    const reportResponse = await axiosService.get(url.href, {
      responseType: 'arraybuffer',
      headers: {
        Authorization: `Bearer ${authorization}`,
        'datalife-refresh-signature': refreshToken,
      },
    })

    fs.writeFile(
      `${REPORTS_PATH}/${new Date().getTime()}.xlsx`,
      reportResponse.data,
      'utf8',
      (error) => {
        if (error) {
          throw new Error('Erro ao salvar arquivo')
        }
      },
    )

    await kafkaProducer.execute({
      topic: 'SENDING_EMAIL',
      payload: {
        deliveryTo,
        report: reportResponse.data,
      },
    })
  } catch (error) {
    console.log(error)
  }
}
