import axios, { AxiosError, AxiosRequestHeaders } from 'axios'

import { env } from '@/env'

const axiosService = axios.create({
  baseURL: env.DATALIFE_API,
})

interface BaseServiceHeaders extends AxiosRequestHeaders {
  'datalife-refresh-signature': string
}

const refreshToken = async (refreshTokenParam: string) => {
  const response = await axiosService.post(`${env.DATALIFE_API}/renewToken`, {
    token: refreshTokenParam,
  })
  const { token } = response.data

  axiosService.defaults.headers.common.Authorization = `Bearer ${token}`
}

axiosService.interceptors.request.use((config) => config)

axiosService.interceptors.response.use(
  (response) => {
    return response
  },
  async (error: AxiosError) => {
    if (error.response?.status === 401) {
      const originalRequest = error.config
      const { 'datalife-refresh-signature': token } =
        originalRequest?.headers as BaseServiceHeaders
      await refreshToken(token)

      return axiosService({ ...originalRequest })
    }
  },
)

export { axiosService }
