import { kafka } from '.'

type Topic = string

type Payload = {
  report: Buffer
  deliveryTo: string
}

interface KafkaSendMessageProps {
  topic: Topic
  payload: Payload
}

export class KafkaSendMessage {
  async execute({ topic, payload }: KafkaSendMessageProps) {
    const producer = kafka.producer()
    await producer.connect()
    await producer.send({
      topic,
      messages: [{ value: JSON.stringify(payload) }],
    })
    await producer.disconnect()
  }
}
