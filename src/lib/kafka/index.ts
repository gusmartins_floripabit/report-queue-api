import { Kafka, logLevel } from 'kafkajs'

import { env } from '@/env'

const kafka = new Kafka({
  brokers: [env.KAFKA_BROKER],
  ssl: true,
  sasl: {
    mechanism: 'scram-sha-256',
    username: env.KAFKA_USERNAME,
    password: env.KAFKA_PASSWORD,
  },
  logLevel: logLevel.ERROR,
})

export { kafka }
