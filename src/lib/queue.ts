import path from 'node:path'

import Queue from 'bull'

import { env } from '@/env'

interface ReportQueueData {
  deliveryTo: string
  payload: {
    filters: Record<string, string>
    reportPathName: string
  }
  headers: {
    authorization: string
    refreshToken: string
  }
}

const REPORT_QUEUE = new Queue<ReportQueueData>('REPORTS', {
  redis: { host: env.REDIS_HOST, port: env.REDIS_PORT },
  defaultJobOptions: {
    delay: 5000,
  },
})

const PROCESSOR_PATH_REPORT_QUEUE = path.join(
  __dirname,
  '..',
  'processor-report-queue.ts',
)

REPORT_QUEUE.on('error', (error) => console.error(`JOB ERROR: ${error}`))
REPORT_QUEUE.on('failed', (job, error) =>
  console.error(`Job: ${job.id} FAILED BY ${error}`),
)
REPORT_QUEUE.on('completed', (job) => console.log('JOB COMPLETED', job.id))

export { PROCESSOR_PATH_REPORT_QUEUE, REPORT_QUEUE, ReportQueueData }
