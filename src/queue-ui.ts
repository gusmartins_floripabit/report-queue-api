import { createBullBoard } from '@bull-board/api'
import { BullAdapter } from '@bull-board/api/bullAdapter'
import { FastifyAdapter } from '@bull-board/fastify'

import { REPORT_QUEUE } from './lib/queue'

const serverAdapter = new FastifyAdapter()
createBullBoard({
  queues: [new BullAdapter(REPORT_QUEUE)],
  serverAdapter,
})
serverAdapter.setBasePath('/ui')

const queueUI = serverAdapter.registerPlugin()

export { queueUI }
