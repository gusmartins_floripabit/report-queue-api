import { FastifyReply, FastifyRequest } from 'fastify'
import { z } from 'zod'

import { REPORT_QUEUE } from '@/lib/queue'
import { CreateReportUseCase } from '@/use-cases/create-report'

const requestBodyShema = z.object({
  deliveryTo: z.string().email(),
  payload: z.object({
    filters: z.record(z.string(), z.string()),
    reportPathName: z.string(),
  }),
})

export async function handle(request: FastifyRequest, reply: FastifyReply) {
  const authorization = request.headers['datalife-signature'] as string
  const refreshToken = request.headers['datalife-refresh-signature'] as string
  const { deliveryTo, payload } = requestBodyShema.parse(request.body)

  try {
    const useCase = new CreateReportUseCase(REPORT_QUEUE)
    await useCase.execute({
      deliveryTo,
      payload,
      headers: { authorization, refreshToken },
    })
    return reply.status(201).send()
  } catch (error) {
    return reply.status(400).send(error)
  }
}
