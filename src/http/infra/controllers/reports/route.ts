import { FastifyInstance } from 'fastify'

import { handle } from './create-report'

export async function reportRoutes(app: FastifyInstance) {
  // app.addHook('onRequest', verifySignatureUpstash)

  app.post('/report', handle)
}
