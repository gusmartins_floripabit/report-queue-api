import cors from '@fastify/cors'
import fastify from 'fastify'
import { ZodError } from 'zod'

import { env } from './env'
import { reportRoutes } from './http/infra/controllers/reports/route'
import { queueUI } from './queue-ui'

const app = fastify()

// fix-me to production
app.register(cors, {
  origin: '*',
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
})

app.register(queueUI, { prefix: '/ui', basePath: '/' })

app.register(reportRoutes)

app.setErrorHandler((error, _, reply) => {
  if (error instanceof ZodError) {
    return reply
      .status(400)
      .send({ message: 'Validation error', issues: error.format() })
  }

  if (env.NODE_ENV !== 'production') {
    console.error(error)
  }

  return reply.status(500).send({ message: 'Internal server error' })
})

export { app }
