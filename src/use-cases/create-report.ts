import { Queue } from 'bull'

import { ReportQueueData } from '@/lib/queue'

interface CreateReportRequest {
  deliveryTo: string
  payload: {
    filters: Record<string, string>
    reportPathName: string
  }
  headers: {
    authorization: string
    refreshToken: string
  }
}

export class CreateReportUseCase {
  constructor(private queue: Queue<ReportQueueData>) {}

  async execute({ deliveryTo, payload, headers }: CreateReportRequest) {
    await this.queue.add({ deliveryTo, payload, headers })
  }
}
